package com.softuni.residentevil.repositories;

import com.softuni.residentevil.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
    Role findRoleByAuthority (String authority);
    Role findRoleById (String id);
}
