package com.softuni.residentevil.repositories;

import com.softuni.residentevil.entities.Virus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public interface VirusRepository extends JpaRepository <Virus, String> {
    Virus findVirusById(String id);
}
