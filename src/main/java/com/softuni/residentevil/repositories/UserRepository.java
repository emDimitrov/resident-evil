package com.softuni.residentevil.repositories;

import com.softuni.residentevil.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findByUsername(String username);
    User findUserById (String id);
}
