package com.softuni.residentevil.entities;

import com.softuni.residentevil.enums.Magnitude;
import com.softuni.residentevil.enums.Mutation;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "viruses")
public class Virus {

    private String name;
    private String description;
    private String sideEffect;
    private String creator;
    private boolean isDeadly;
    private boolean isCurable;
    private Mutation mutation;
    private int turnoverRate;
    private int hoursUntilMutation;
    private Magnitude magnitude;
    private LocalDate releasedOnDate;
    private Set<Capital> capitals;

    private String id;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "virus_id", nullable = false, unique = true, updatable = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", columnDefinition = "TEXT", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "side_effect", nullable = false)
    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    @Column(name = "creator", nullable = false)
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Column(name = "is_deadly", nullable = false)
    public boolean isDeadly() {
        return isDeadly;
    }

    public void setDeadly(boolean deadly) {
        isDeadly = deadly;
    }

    @Column(name = "is_curable", nullable = false)
    public boolean isCurable() {
        return isCurable;
    }

    public void setCurable(boolean curable) {
        isCurable = curable;
    }

    @Column(name = "mutation", nullable = false)
    @Enumerated(EnumType.STRING)
    public Mutation getMutation() {
        return mutation;
    }

    public void setMutation(Mutation mutation) {
        this.mutation = mutation;
    }

    @Column(name = "turnover_rate", nullable = false)
    public int getTurnoverRate() {
        return turnoverRate;
    }

    public void setTurnoverRate(int turnoverRate) {
        this.turnoverRate = turnoverRate;
    }

    @Column(name = "hours_until_mutation", nullable = false)
    public int getHoursUntilMutation() {
        return hoursUntilMutation;
    }

    public void setHoursUntilMutation(int hoursUntilMutation) {
        this.hoursUntilMutation = hoursUntilMutation;
    }

    @Column(name = "magnitude", nullable = false)
    @Enumerated(EnumType.STRING)
    public Magnitude getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(Magnitude magnitude) {
        this.magnitude = magnitude;
    }

    @Column(name = "released_on_date", nullable = false)
    public LocalDate getReleasedOnDate() {
        return releasedOnDate;
    }

    public void setReleasedOnDate(LocalDate releasedOnDate) {
        this.releasedOnDate = releasedOnDate;
    }

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="virus_capital",
            joinColumns=@JoinColumn(name="virus_id"),
            inverseJoinColumns=@JoinColumn(name="capital_id"))
    public Set<Capital> getCapitals() {
        return capitals;
    }

    public void setCapitals(Set<Capital> capitals) {
        this.capitals = capitals;
    }
}

