package com.softuni.residentevil.controllers;

import com.softuni.residentevil.services.VirusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MapController {
    private VirusService virusService;

    @Autowired
    public MapController(VirusService virusService) {
        this.virusService = virusService;
    }

    @GetMapping("/map")
    public String getMapPage(Model model) {
        String geoJson = null;
        model.addAttribute("geoJson", geoJson);
        return "map";

    }
}
