package com.softuni.residentevil.controllers;

import com.softuni.residentevil.models.binding.user.UserEditBindingModel;
import com.softuni.residentevil.models.service.UserServiceModel;
import com.softuni.residentevil.models.view.user.UserEditViewModel;
import com.softuni.residentevil.services.RoleService;
import com.softuni.residentevil.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController extends BaseController{

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final RoleService roleService;

    @Autowired
    public AdminController(UserService userService, ModelMapper modelMapper, RoleService roleService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.roleService = roleService;
    }

    @GetMapping("/user/all")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView getAll(){
        return this.view("views/user/all", this.userService.getAll(),"Users");
    }

    @GetMapping("/user/delete/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView delete(@PathVariable String id){

        this.userService.deleteById(id);
        return this.view("views/user/all", null,"Users")
                .addObject("users",this.userService.getAll());
    }

    @GetMapping("/user/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView getEdit(@PathVariable String id, @ModelAttribute UserEditViewModel viewModel){
        modelMapper.map(this.userService.findUserById(id),viewModel);
        //viewModel.setAuthorities(this.userService.getRoles(this.userService.getRoles(viewModel));
        return this.view("views/user/edit", viewModel, "Users")
                .addObject("roles", this.roleService.getAll());
    }

    @PostMapping("/user/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView edit(@PathVariable String id, @ModelAttribute UserEditBindingModel bindingModel){
        this.userService.update(id, bindingModel);
        return this.view("views/user/all", null, "Users");
    }
}
