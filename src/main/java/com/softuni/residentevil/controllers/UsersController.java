package com.softuni.residentevil.controllers;

import com.softuni.residentevil.models.binding.user.UserRegisterBindingModel;
import com.softuni.residentevil.services.RoleService;
import com.softuni.residentevil.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class UsersController extends BaseController {

    private UserService userService;

    @Autowired
    public UsersController(UserService userService, RoleService roleService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getLogin() {
        return this.view("views/user/login",null,"Login");
    }

    @GetMapping("/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getRegister(){
        return this.view("views/user/register");
    }

    @PostMapping("/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView register(@ModelAttribute UserRegisterBindingModel bindingModel){
        if(!bindingModel.getPassword()
                .equals(bindingModel.getConfirmPassword())) {
            return this.view("register");
        }
        this.userService.create(bindingModel);

        return this.view("views/user/login");
    }
}
