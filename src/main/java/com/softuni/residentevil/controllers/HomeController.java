package com.softuni.residentevil.controllers;

import com.softuni.residentevil.customValidations.PreAuthenticate;
import com.softuni.residentevil.entities.Virus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
public class HomeController extends BaseController{
    @GetMapping("/")
    public ModelAndView index(){
        return this.view("views/index", null,"Welcome");
    }

    @GetMapping("/home")
    @PreAuthorize("!isAnonymous()")
    public ModelAndView home(Principal principal) {
        return this.view("views/home", principal.getName(),"Resident Evil");
    }
}
