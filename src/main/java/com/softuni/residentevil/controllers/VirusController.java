package com.softuni.residentevil.controllers;

import com.softuni.residentevil.entities.Virus;
import com.softuni.residentevil.models.binding.virus.CreateVirusBindingModel;
import com.softuni.residentevil.models.binding.virus.EditVirusBindingModel;
import com.softuni.residentevil.services.CapitalService;
import com.softuni.residentevil.services.VirusService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/virus")
public class VirusController extends BaseController {

    private final VirusService virusService;
    private final CapitalService capitalService;
    private final ModelMapper modelMapper;

    @Autowired
    public VirusController(VirusService virusService, CapitalService capitalService, ModelMapper modelMapper) {
        this.virusService = virusService;
        this.capitalService = capitalService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/create")
    @PreAuthorize("hasAnyAuthority('ROOT', 'MODERATOR', 'ADMIN')")
    public ModelAndView getCreate(@ModelAttribute CreateVirusBindingModel createVirusBindingModel){
        return this.view("views/virus/add", null,"Create Virus")
                .addObject("affectedCapitals", this.capitalService.getAll());
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ROOT', 'MODERATOR', 'ADMIN')")
    public ModelAndView create(@Valid @ModelAttribute CreateVirusBindingModel createVirusBindingModel,
                               BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return this.view("views/virus/add", createVirusBindingModel,"Create Virus")
                    .addObject("affectedCapitals", this.capitalService.getAll());
        }
        this.virusService.create(createVirusBindingModel);

        return this.view("views/virus/all", null, "All Viruses");
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROOT', 'MODERATOR', 'ADMIN', 'USER')")
    public ModelAndView getAll(){
        return this.view("views/virus/all", this.virusService.getAll(),"Viruses");
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'MODERATOR', 'ADMIN')")
    public ModelAndView getEdit(@PathVariable String id,
                                @ModelAttribute EditVirusBindingModel bindingModel){

        Virus virus = this.virusService.findVirusById(id);
        modelMapper.map(virus, bindingModel);
        bindingModel.setAffectedCapitals(this.virusService.getCapitalIds(virus));

        return this.view("views/virus/edit", bindingModel, "Edit Virus")
                .addObject("capitals", this.capitalService.getAll());
    }

    @PostMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'MODERATOR', 'ADMIN')")
    public ModelAndView editUser(@PathVariable String id, @Valid @ModelAttribute EditVirusBindingModel bindingModel,
                                 BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return this.view("views/virus/edit", bindingModel,"Edit Virus")
                    .addObject("capitals", this.capitalService.getAll());
        }

        this.virusService.update(id, bindingModel);

        return this.view("views/virus/all", this.virusService.getAll(),"Viruses");
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'MODERATOR', 'ADMIN')")
    public ModelAndView delete(@PathVariable String id){
        this.virusService.deleteById(id);
        return view("views/virus/all", this.virusService.getAll(), "Viruses");
    }
}
