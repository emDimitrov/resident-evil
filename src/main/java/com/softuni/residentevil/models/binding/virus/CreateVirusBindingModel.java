package com.softuni.residentevil.models.binding.virus;

import com.softuni.residentevil.customValidations.DateBeforeToday;
import com.softuni.residentevil.enums.Magnitude;
import com.softuni.residentevil.enums.Mutation;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

public class CreateVirusBindingModel implements VirusBindingModel {
    private String name;
    private String description;
    private String sideEffect;
    private String creator;
    private boolean isDeadly;
    private boolean isCurable;
    private Mutation mutation;
    private int turnoverRate;
    private int hoursUntilMutation;
    private Magnitude magnitude;
    private Set<Long> affectedCapitals;

    @DateBeforeToday
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate releasedOnDate;

    @NotNull
    @Size(min = 3, max = 10)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    @Size(min = 5, max = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    @Size(max = 50)
    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    @NotNull
    @Pattern(regexp = "^corp$|Corp$")
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public boolean isDeadly() {
        return isDeadly;
    }

    public void setDeadly(boolean deadly) {
        isDeadly = deadly;
    }

    public boolean isCurable() {
        return isCurable;
    }

    public void setCurable(boolean curable) {
        isCurable = curable;
    }

    @NotNull
    public Mutation getMutation() {
        return mutation;
    }

    public void setMutation(Mutation mutation) {
        this.mutation = mutation;
    }

    @NotNull
    @Range(min=0,max=100)
    public int getTurnoverRate() {
        return turnoverRate;
    }

    public void setTurnoverRate(int turnoverRate) {
        this.turnoverRate = turnoverRate;
    }

    @NotNull
    @Range(min=1,max=12)
    public int getHoursUntilMutation() {
        return hoursUntilMutation;
    }

    public void setHoursUntilMutation(int hoursUntilMutation) {
        this.hoursUntilMutation = hoursUntilMutation;
    }

    public Magnitude getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(Magnitude magnitude) {
        this.magnitude = magnitude;
    }

    @NotNull
    @Size(min = 1)
    public Set<Long> getAffectedCapitals() {
        return affectedCapitals;
    }

    public void setAffectedCapitals(Set<Long> affectedCapitals) {
        this.affectedCapitals = affectedCapitals;
    }

    public LocalDate getReleasedOnDate() {
        return releasedOnDate;
    }

    public void setReleasedOnDate(LocalDate releasedOnDate) {
        this.releasedOnDate = releasedOnDate;
    }
}
