package com.softuni.residentevil.models.binding.user;

import java.util.Set;

public interface UserModel {
    public Set<String> getAuthorities();
}
