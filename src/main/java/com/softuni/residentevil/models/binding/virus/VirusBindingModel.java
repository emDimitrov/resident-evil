package com.softuni.residentevil.models.binding.virus;

import com.softuni.residentevil.entities.Capital;

import java.util.Set;

public interface VirusBindingModel {
    Set<Long> getAffectedCapitals();
}
