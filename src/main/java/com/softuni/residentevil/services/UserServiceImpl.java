package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Role;
import com.softuni.residentevil.entities.User;
import com.softuni.residentevil.enums.UserRole;
import com.softuni.residentevil.models.binding.user.UserEditBindingModel;
import com.softuni.residentevil.models.binding.user.UserModel;
import com.softuni.residentevil.models.binding.user.UserRegisterBindingModel;
import com.softuni.residentevil.models.service.UserServiceModel;
import com.softuni.residentevil.models.view.user.UserEditViewModel;
import com.softuni.residentevil.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepo;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleService roleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepo, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder, RoleService roleService) {
        this.userRepo = userRepo;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleService = roleService;
    }

    @Override
    public boolean create(UserRegisterBindingModel bindingModel) {
        User user = modelMapper.map(bindingModel, User.class);
        user.setPassword(bCryptPasswordEncoder.encode(bindingModel.getPassword()));

        if (!this.getAll().isEmpty()){
            Set<Role> authorities = new HashSet<Role>(){{
                add(roleService.getRoleByUserRole(UserRole.USER));
            }};
            user.setAuthorities(authorities);
        }

        return this.userRepo.save(user) != null;
    }

    @Override
    public Set<User> getAll() {
        return this.userRepo
                .findAll()
                .stream()
                .collect(Collectors.toSet());
    }

    @Override
    public UserServiceModel findUserById(String id) {
        User user = this.userRepo.findById(id).orElse(null);
        return this.modelMapper.map(user, UserServiceModel.class);
    }

    @Override
    public void deleteById(String id) {
        UserEditBindingModel bindingModel= new UserEditBindingModel() ;
        this.modelMapper.map(this.userRepo.findUserById(id), bindingModel);

        if (!isRoot(bindingModel)) {
            this.userRepo.deleteById(id);
        }
    }

    @Override
    public UserServiceModel getUserByUsername(String username) {
        return this.modelMapper.map(
                this.userRepo.findByUsername(username), UserServiceModel.class);
    }

    @Override
    public boolean update(String id, UserEditBindingModel bindingModel) {
        User user = this.userRepo.findById(id).orElse(null);
        modelMapper.map(bindingModel, user);
        if (isRoot(bindingModel)) {
            return this.userRepo.save(user) != null;
        }

        return false;
    }

    @Override
    public boolean isRoot(UserEditBindingModel bindingModel) {
        Set<String> authorities = bindingModel.getAuthorities();
        for (String role : authorities) {
            if (role == "ROOT") {
                return false;
            }
        }

        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User foundUser = this.userRepo
                .findByUsername(username)
                .orElse(null);

        if (foundUser == null) {
            throw new UsernameNotFoundException("Username not found.");
        }

        return foundUser;
    }

    @Override
    public Set<Role> getRoles(UserModel userModel){
        Set<Role> roles= new HashSet<>();
        for (String roleId : userModel.getAuthorities()) {
            roles.add(roleService.getRoleById(roleId));
        }

        return roles;
    }

    @Override
    public Set<String> getRolesIds(UserServiceModel userServiceModel){
        Set<String> roles = new HashSet<>();
        for (Role role : userServiceModel.getAuthorities()) {
            roles.add(role.getId());
        }
        return  roles;
    }
}
