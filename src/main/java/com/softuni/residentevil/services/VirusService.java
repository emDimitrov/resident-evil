package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Capital;
import com.softuni.residentevil.entities.Virus;
import com.softuni.residentevil.models.binding.virus.CreateVirusBindingModel;
import com.softuni.residentevil.models.binding.virus.EditVirusBindingModel;
import com.softuni.residentevil.models.binding.virus.VirusBindingModel;

import java.util.Set;

public interface VirusService {
    boolean create(CreateVirusBindingModel createVirusBindingModel);

    Set<Virus> getAll();

    Virus findVirusById(String id);

    void deleteById(String id);

    boolean update(String id, EditVirusBindingModel editVirusBindingModel);

    Set<Capital> getCapitals(VirusBindingModel bindingModel);

    Set<Long> getCapitalIds(Virus virus);
}
