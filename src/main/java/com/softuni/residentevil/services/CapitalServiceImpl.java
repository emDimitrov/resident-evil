package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Capital;
import com.softuni.residentevil.repositories.CapitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CapitalServiceImpl implements CapitalService {

    private CapitalRepository capitalRepo;

    @Autowired
    public CapitalServiceImpl(CapitalRepository capitalRepo) {
        this.capitalRepo = capitalRepo;
    }

    @Override
    public Set<Capital> getAll() {
        return this.capitalRepo.findAll()
                .stream()
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Capital getById(long id) {
        return this.capitalRepo.findById(id).orElse(null);
    }
}
