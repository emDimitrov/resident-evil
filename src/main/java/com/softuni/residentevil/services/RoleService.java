package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Role;
import com.softuni.residentevil.enums.UserRole;

import java.util.Set;

public interface RoleService {
    boolean create(Role role);

    Role getRoleByUserRole(UserRole userRole);

    Role getRoleById(String id);

    Set<Role> getAll();
}
