package com.softuni.residentevil.services;


import com.softuni.residentevil.entities.Role;
import com.softuni.residentevil.entities.User;
import com.softuni.residentevil.models.binding.user.UserEditBindingModel;
import com.softuni.residentevil.models.binding.user.UserModel;
import com.softuni.residentevil.models.binding.user.UserRegisterBindingModel;
import com.softuni.residentevil.models.service.UserServiceModel;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Set;

public interface UserService extends UserDetailsService {
    boolean create(UserRegisterBindingModel bindingModel);

    Set<User> getAll();

    UserServiceModel findUserById(String id);

    void deleteById(String id);

    UserServiceModel getUserByUsername(String username);

    boolean update(String id, UserEditBindingModel bindingModel);

    boolean isRoot(UserEditBindingModel bindingModel);

    Set<String> getRolesIds(UserServiceModel userServiceModel);

    Set<Role> getRoles(UserModel userModel);
}
