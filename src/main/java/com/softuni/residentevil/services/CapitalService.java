package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Capital;

import java.util.Set;

public interface CapitalService {
    Set<Capital> getAll();

    Capital getById(long id);
}
