package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Capital;
import com.softuni.residentevil.entities.Virus;
import com.softuni.residentevil.models.binding.virus.CreateVirusBindingModel;
import com.softuni.residentevil.models.binding.virus.EditVirusBindingModel;
import com.softuni.residentevil.models.binding.virus.VirusBindingModel;
import com.softuni.residentevil.repositories.VirusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VirusServiceImpl implements VirusService {

    private final VirusRepository virusRepo;
    private final CapitalService capitalService;

    private final ModelMapper modelMapper;

    @Autowired
    public VirusServiceImpl(VirusRepository virusRepo, CapitalService capitalService, ModelMapper modelMapper) {
        this.virusRepo = virusRepo;
        this.capitalService = capitalService;
        this.modelMapper = modelMapper;
    }

    @Override
    public boolean create(CreateVirusBindingModel createVirusBindingModel) {
        Virus virus = new Virus();
        modelMapper.map(createVirusBindingModel, virus);
        virus.setCapitals(getCapitals(createVirusBindingModel));

        return virusRepo.save(virus) != null;
    }

    @Override
    public Set<Virus> getAll() {
        return new HashSet<>(this.virusRepo
                .findAll()
                .stream()
                .collect(Collectors.toUnmodifiableSet()));
    }

    @Override
    public Virus findVirusById(String id) {
        return this.virusRepo.findById(id).orElse(null);
    }

    @Override
    public void deleteById(String id) {
         this.virusRepo.deleteById(id);
    }

    @Override
    public boolean update(String id, EditVirusBindingModel editVirusBindingModel) {
        Virus virus = this.findVirusById(id);
        modelMapper.map(editVirusBindingModel, virus);
        virus.setCapitals(getCapitals(editVirusBindingModel));

        return this.virusRepo.save(virus) != null;
    }

    public Set<Capital> getCapitals(VirusBindingModel bindingModel){
        Set<Capital> capitals= new HashSet<>();
        for (Long capitalId : bindingModel.getAffectedCapitals()) {
            capitals.add(capitalService.getById(capitalId));
        }

        return capitals;
    }

    public Set<Long> getCapitalIds(Virus virus){
        Set<Long> capitals = new HashSet<>();
        for (Capital capital : virus.getCapitals()) {
            capitals.add(capital.getId());
        }
        return  capitals;
    }
}
