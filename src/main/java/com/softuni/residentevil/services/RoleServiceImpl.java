package com.softuni.residentevil.services;

import com.softuni.residentevil.entities.Role;
import com.softuni.residentevil.enums.UserRole;
import com.softuni.residentevil.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepo;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public boolean create(Role role) {
        return this.roleRepo.save(role) != null;
    }

    @Override
    public Role getRoleByUserRole(UserRole userRole) {
        return this.roleRepo.findRoleByAuthority(userRole.name());
    }

    @Override
    public Role getRoleById(String id) {
        return this.roleRepo.findRoleById(id);
    }

    @Override
    public Set<Role> getAll() {
        return this.roleRepo.findAll().stream().collect(Collectors.toSet());
    }
}
