package com.softuni.residentevil.enums;

public enum UserRole {
    USER,
    MODERATOR,
    ADMIN,
    ROOT
}
