package com.softuni.residentevil.config;

import com.softuni.residentevil.entities.Role;
import com.softuni.residentevil.entities.User;
import com.softuni.residentevil.enums.UserRole;
import com.softuni.residentevil.models.binding.user.UserRegisterBindingModel;
import com.softuni.residentevil.services.RoleService;
import com.softuni.residentevil.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
public class DatabaseSeeder {

    private final UserService userService;
    private final JdbcTemplate jdbcTemplate;
    private final RoleService roleService;

    @Autowired
    public DatabaseSeeder(JdbcTemplate jdbcTemplate, UserService userService, RoleService roleService) {
        this.jdbcTemplate = jdbcTemplate;
        this.userService = userService;
        this.roleService = roleService;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedRolesTable();
        seedUsersTable();
    }

    private void seedRolesTable() {
        String sql = "SELECT authority FROM roles";
        List<String> result = jdbcTemplate.query(sql, (resultSet, rowNum) -> null);
        if(result == null || result.size() <= 0) {
            Role root = new Role(UserRole.ROOT.name());
            Role admin = new Role(UserRole.ADMIN.name());
            Role moderator = new Role(UserRole.MODERATOR.name());
            Role user = new Role(UserRole.USER.name());

            roleService.create(root);
            roleService.create(admin);
            roleService.create(user);
            roleService.create(moderator);
        }
    }

    private void seedUsersTable() {
        String sql = "SELECT * FROM Users";
        List<User> result = jdbcTemplate.query(sql, (resultSet, rowNum) -> null);

        if(result == null || result.size() <= 0) {
            Set<Role> authorities = new HashSet<Role>(){{
                add(roleService.getRoleByUserRole(UserRole.ROOT));
            }};

            UserRegisterBindingModel rootUser = new UserRegisterBindingModel("admin","admin@mail.bg","admin", authorities);
            this.userService.create(rootUser);
        }
    }
}
