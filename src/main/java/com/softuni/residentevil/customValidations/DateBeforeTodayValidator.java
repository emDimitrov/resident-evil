package com.softuni.residentevil.customValidations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;


public class DateBeforeTodayValidator implements ConstraintValidator<DateBeforeToday, LocalDate> {

    @Override
    public boolean isValid(LocalDate date, ConstraintValidatorContext constraintValidatorContext) {
        if (date == null) {
            return false;
        }
        LocalDate today = LocalDate.now();
        return date.isBefore(today);
    }
}

